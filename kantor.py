from collections import namedtuple
from dataclasses import dataclass
from typing import Dict

Amount = namedtuple('Amount', 'sum abbr')
Currency = namedtuple('Currency', 'name abbr rate')
EUR = Currency('euro', 'EUR', 4.32)
USD = Currency('dollar', 'USD', 3.93)
CHF = Currency('franc', 'CHF', 3.95)
PLN = Currency('zloty', 'PLN', 1.00)


@dataclass
class CurrencyAccount:
    abbr: str
    balance: float = 0.
    debit: float = 0.

    class NotEnougCash(Exception):
        """Raised when not enough money on account."""

    @property
    def total_available(self) -> float:
        return self.balance + self.debit

    def pay_out(self, amount: float) -> Amount:
        if amount <= self.total_available:
            self.balance -= amount
            return Amount(amount, self.abbr)
        else:
            raise self.NotEnougCash(
                f'{amount} is more than can be paid out'
            )

    def pay_in(self, amount: float):
        self.balance += amount


@dataclass
class ExchangeOffice:
    spread: float = 2.4
    currencies: Dict[str, Currency] = None

    @staticmethod
    def calculate_spread(exchange_rate: float, spread: float) -> float:
        return exchange_rate * spread / 100

    def __post_init__(self):
        if self.currencies is None:
            self.currencies = {'EUR': EUR,
                               'USD': USD,
                               'CHF': CHF,
                               'PLN': PLN}

    def _exchange_to_pln(self, amount: Amount) -> Amount:
        spread_value = self.calculate_spread(self.currencies[amount.abbr].rate, self.spread)
        return Amount(amount.sum * (self.currencies[amount.abbr].rate - spread_value / 2), 'PLN')

    def _exchange_from_pln(self, amount: Amount, target: Currency) -> Amount:
        spread_value = self.calculate_spread(self.currencies[target.abbr].rate, self.spread)
        return Amount(amount.sum / (self.currencies[target.abbr].rate + spread_value / 2), target.abbr)

    def exchange(self, amount: Amount, target: Currency) -> Amount:
        if target.abbr == 'PLN':
            return self._exchange_to_pln(amount)
        else:
            if amount.abbr == 'PLN':
                return self._exchange_from_pln(amount, target)
            else:
                temp = self._exchange_to_pln(amount)
                return self._exchange_from_pln(temp, target)


if __name__ == '__main__':
    account = CurrencyAccount('PLN', 10_000, debit=2_000)
    print(account.total_available)
    account.pay_in(2_000)
    print(account.total_available)
    payed_out = account.pay_out(5_450)
    print(payed_out)
    print(account.total_available)
    # account.pay_out(100_000_000)
    kantor = ExchangeOffice()
    amount_1 = Amount(100, 'EUR')
    amount_2 = Amount(100, 'USD')
    amount_3 = Amount(100, 'PLN')
    print(f'{kantor.exchange(amount_1, PLN).sum:.2f}')
    print(kantor.exchange(amount_1, PLN).abbr)
    print(f'{kantor.exchange(amount_2, PLN).sum:.2f}')
    print(kantor.exchange(amount_2, PLN).abbr)
    print(f'{kantor.exchange(amount_3, EUR).sum:.2f}')
    print(kantor.exchange(amount_3, EUR).abbr)
    print(f'{kantor.exchange(amount_2, EUR).sum:.2f}')
    print(kantor.exchange(amount_2, EUR).abbr)
